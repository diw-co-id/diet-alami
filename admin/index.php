<?php
include("connect_server.php");

if($_COOKIE['id_admin'] != 0)
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php include("connect_server.php"); ?>

		<?php $judul = 'Dashboard (Admin Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <b>Dashboard</b></li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> Dashboard</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="alert alert-block alert-info">
									<i class="ace-icon fa fa-smile-o blue fa-fw"></i>

									Selama datang di
									<strong class="blue">
										Admin Panel <?php echo"$row_setting[name_website]"; ?>
									</strong>, database dan pengaturan bisa ditambah, diubah, dan di hapus oleh anda.
								</div>

								<div class="row">
								    
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-info">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-shopping-cart fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_pemesanan"; ?></div>
														<div>Total Pemesanan</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/pemesanan">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Pemesanan</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
								    
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-success">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-truck fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_bukti_pengiriman"; ?></div>
														<div>Total Bukti Pengiriman</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/bukti_pengiriman">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Bukti Pengiriman</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-warning">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-comment-o fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_testimoni"; ?></div>
														<div>Total Testimoni</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/testimoni">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Testimoni</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-danger">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-picture-o fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_foto_ramping_herbal"; ?></div>
														<div>Total Foto Ramping Herbal</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/foto_ramping_herbal">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Foto Ramping Herbal</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
								</div>
								<div class="row">
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-info">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-question-circle fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_faq"; ?></div>
														<div>Total FAQ</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/faq">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data FAQ</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-success">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-phone fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_kontak"; ?></div>
														<div>Total Kontak</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/kontak">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Kontak</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-warning">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-newspaper-o fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_artikel"; ?></div>
														<div>Total Artikel</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/artikel">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Artikel</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-danger">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-sliders fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_slider"; ?></div>
														<div>Total Slider</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/slider">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Slider</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
								</div>
								<div class="row">
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-info">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-money fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_biaya_pengiriman"; ?></div>
														<div>Total Biaya Pengiriman</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/biaya_pengiriman">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Biaya Pengiriman</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="col-lg-3 col-md-6">
										<div class="panel panel-success">
											<div class="panel-heading">
												<div class="row">
													<div class="col-xs-3">
														<i class="fa fa-user fa-5x"></i>
													</div>
													<div class="col-xs-9 text-right">
														<div class="lead"><?php echo"$row_total_admin"; ?></div>
														<div>Total Admin</div>
													</div>
												</div>
											</div>
											<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/admin">
												<div class="panel-footer">
													<span class="pull-left">Lihat Data Admin</span>
													<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
													<div class="clearfix"></div>
												</div>
											</a>
										</div>
									</div>
								</div><!-- /.row -->
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>