        <footer>
			<div class="background-primary padding text-center">
			    <h6 class="headline text-thin text-s-size-30">
					Tinggalkan Body Lama Anda<br>
					Waktunya Memiliki Tubuh Ideal Dan Sehat<br>
					Sebelum Berat Badan Bertambah</b>
				</h6>
				<!--<br>
				<a draggable="false" href="<?php echo"$row_setting[facebook]"; ?>"><i class="icon-facebook_circle icon2x text-white"></i></a> 
				<a draggable="false" href="<?php echo"$row_setting[twitter]"; ?>"><i class="icon-twitter_circle icon2x text-white"></i></a> 
				<a draggable="false" href="<?php echo"$row_setting[google]"; ?>"><i class="icon-google_plus_circle icon2x text-white"></i></a> 
				<a draggable="false" href="<?php echo"$row_setting[instagram]"; ?>"><i class="icon-instagram_circle icon2x text-white"></i></a>-->
			</div>
	  
			<section class="section background-dark" style="margin-bottom:-170px;">
				<div class="line">
					<div class="margin">
						<div class="s-12 m-12 l-4">
							<h4 class="text-uppercase text-strong">Tentang Diet Alami</h4>
							<p>
							    Pelangsing Penurun Berat Badan Cepat Dan Aman 
                                Best Seller Merek Terdaftar Solusi Terbaik Untuk Tubuh Ideal
                            </p>
                            <br>
							<p class="text-size-20"><em>"Tinggalkan Body Lama Anda Waktunya Memiliki Tubuh Ideal Dan Sehat Sebelum Berat Badan Bertambah."</em></p>
							<br>
						    <img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-semua-bank.png" alt="Logo Semua Bank - <?php echo"$row_setting[title]"; ?>">
						    <br><br>
							<!--<div class="line">
								<h4 class="text-uppercase text-strong margin-top-30">About Our Company</h4>
								<div class="margin">
									<div class="s-12 m-12 l-4 margin-m-bottom">
										<a class="image-hover-zoom" href="/"><img src="img/blog-04.jpg" alt=""></a>
									</div>
									<div class="s-12 m-12 l-8 margin-m-bottom">
										<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
										<a class="text-more-info text-primary-hover" href="/">Read more</a>
									</div>
								</div>
							</div>-->
						</div>
						
						<div class="s-12 m-12 l-4">
							<h4 class="text-uppercase text-strong">Navigasi Halaman</h4>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-home text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/"><p>Home</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-truck text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/bukti-pengiriman"><p>Bukti Pengiriman</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-comment-o text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/testimoni"><p>Testimoni</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-picture-o text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/foto-ramping-herbal"><p>Foto Ramping Herbal</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-question-circle text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/faq"><p>FAQ</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-shopping-cart text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/pemesanan"><p>Pemesanan</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-phone text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/hubungi-kami"><p>Hubungi Kami</p></a></div>
    						</div>
							<!--<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-sticky-note-o text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/pelangsing"><p><b>Artikel</b> : Pelangsing</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-sticky-note-o text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/diet"><p><b>Artikel</b> : Diet</p></a></div>
    						</div>-->
    						<br><br>
						</div>
						
						<div class="s-12 m-12 l-4">
							<h4 class="text-uppercase text-strong">Hubungi Kami</h4>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><!--<i class="fa fa-mobile text-primary text-size-14" style="margin-top:5px;"></i>--><img draggable="false" class="text-center" src="<?php echo"$row_setting[domain]"; ?>/images/icon-bbm.png" style="width:20px;"></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" target="_blank" href="http://pin.bbm.com/D76216A4"><p><b>BBM</b> : D76216A4</p></a></div>
    						</div>
							<!--<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-mobile text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" target="_blank" href="https://line.me/ti/p/~@omz3541w"><p><b>Line</b> : diet.alami.id</p></a></div>
    						</div>-->
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-inbox text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" target="_blank" href="sms:+6281517135454?&body=Halo, Saya ingin memesan produk herbalnya"><p><b>SMS</b> : 0815 1713 5454</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-phone text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="tel:+6281517135454"><p><b>CALL</b> : 0815 1713 5454</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-whatsapp text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" target="_blank" href="https://api.whatsapp.com/send?phone=6281517135454&text=Halo, Saya ingin memesan produk herbalnya"><p><b>Whatsapp</b> : 0815 1713 5454</p></a></div>
    						</div>
							<div class="line">
								<div class="s-1 m-1 l-1 text-center"><i class="fa fa-envelope-o text-primary text-size-14" style="margin-top:5px;"></i></div>
    							<div class="s-11 m-11 l-11 margin-bottom-10"><a draggable="false" href="mailto:info@dietalami.id"><p><b>Email</b> : info@dietalami.id</p></a></div>
    						</div>
    						<br><br>
						</div>
						
						<!--<div class="s-12 m-12 l-4">
							<h4 class="text-uppercase text-strong">Leave a Message</h4>
							<form class="customform text-white">
								<div class="line">
									<div class="margin">
										<div class="s-12 m-12 l-6">
											<input name="email" class="required email border-radius" placeholder="Your e-mail" title="Your e-mail" type="text" />
										</div>
										<div class="s-12 m-12 l-6">
											<input name="name" class="name border-radius" placeholder="Your name" title="Your name" type="text" />
										</div>
									</div>
								</div>
								<div class="s-12">
									<textarea name="message" class="required message border-radius" placeholder="Your message" rows="3"></textarea>
								</div>
								<div class="s-12">
									<button class="submit-form button background-primary border-radius text-white" type="submit">Submit Button</button>
								</div> 
						  </form>
						</div>-->
					</div>
				</div>
			</section>
			
			<section class="padding background-dark" style="margin-top:-50px;">
				<div class="line">
					<div class="s-12 l-12 text-center">
					    <hr>
						<p class="text-size-12" style="color:#FFFFFF;">&copy; Hak Cipta <?php echo date("Y"); ?> &reg; <a draggable="false" style="color:#FFFFFF;" href="<?php echo"$row_setting[domain]"; ?>/" target="_blank"><strong><?php echo"$row_setting[name_website]"; ?></strong></a> &trade;. Seluruh Hak Cipta Dilindungi.</p>
					</div>
				</div>
			</section>
		</footer>
<?php
$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
{
?>
	<br><br>
	<div id="footer_bawah">
	    <div class="row">
    		<div class="s-4 m-4 l-4 text-center" style="margin-top:10px;">
    			<!--<a draggable="false" href="tel:+6287888686800" style="color:#FFFFFF; font-size:20px;"><i style="color:#FFFFFF;" class="fa fa-phone 2x fa-fw" aria-hidden="true"></i><br>Call Now</a>-->
    			<a draggable="false" href="http://pin.bbm.com/D76216A4" style="color:#FFFFFF; font-size:20px;" target="_blank"><center><img draggable="false" class="text-center" src="<?php echo"$row_setting[domain]"; ?>/images/icon-bbm.png" style="width:20px;"></center>Invite BBM</a>
    		</div>
    		<div class="s-4 m-4 l-4 text-center" style="margin-top:10px;">
    			<a draggable="false" href="https://api.whatsapp.com/send?phone=6281517135454&text=Halo, Saya ingin memesan produk herbalnya" style="color:#FFFFFF; font-size:20px;"><i style="color:#FFFFFF;" class="fa fa-whatsapp 2x fa-fw" aria-hidden="true"></i><br>Whatsapp</a>
    		</div>
    		<div class="s-4 m-4 l-4 text-center" style="margin-top:10px;"></div>
    	</div>
	</div>
<?
}
?>
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/responsee.js"></script>
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/owl-carousel/owl.carousel.js"></script>
		<script type="text/javascript" src="<?php echo"$row_setting[domain]"; ?>/js/template-scripts.js"></script>