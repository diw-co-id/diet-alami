			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<ul class="nav nav-list">
					<li class="<?php echo"$link_dasbor"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">
							<i class="menu-icon fa fa-dashboard fa-fw"></i>
							<span class="menu-text">Dashboard</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_pemesanan"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/pemesanan">
							<i class="menu-icon fa fa-shopping-cart fa-fw"></i>
							<span class="menu-text">Pemesanan</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_bukti_pengiriman"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/bukti_pengiriman">
							<i class="menu-icon fa fa-truck fa-fw"></i>
							<span class="menu-text">Bukti Pengiriman</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_testimoni"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/testimoni">
							<i class="menu-icon fa fa-comment-o fa-fw"></i>
							<span class="menu-text">Testimoni</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_foto_ramping_herbal"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/foto_ramping_herbal">
							<i class="menu-icon fa fa-picture-o fa-fw"></i>
							<span class="menu-text">Foto Ramping Herbal</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_faq"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/faq">
							<i class="menu-icon fa fa-question-circle fa-fw"></i>
							<span class="menu-text">FAQ</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_kontak"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/kontak">
							<i class="menu-icon fa fa-phone fa-fw"></i>
							<span class="menu-text">Kontak</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<!--<li class="<?php echo"$link_artikel"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/artikel">
							<i class="menu-icon fa fa-newspaper-o fa-fw"></i>
							<span class="menu-text">Artikel</span>
						</a>
						<b class="arrow"></b>
					</li>-->
					
					<li class="<?php echo"$link_slider"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/slider">
							<i class="menu-icon fa fa-sliders fa-fw"></i>
							<span class="menu-text">Slider</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_biaya_pengiriman"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/biaya_pengiriman">
							<i class="menu-icon fa fa-money fa-fw"></i>
							<span class="menu-text">Biaya Pengiriman</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="<?php echo"$link_admin"; ?>">
						<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/admin">
							<i class="menu-icon fa fa-user fa-fw"></i>
							<span class="menu-text">Admin</span>
						</a>
						<b class="arrow"></b>
					</li>
				</ul>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>