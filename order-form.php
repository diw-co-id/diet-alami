            <div class="section background-white"> 
				<div class="line">
					<div class="margin">
						<div class="s-12 m-12 l-4">
							<h2 class="text-uppercase text-strong margin-bottom-30">Informasi Pemesanan</h2>
							<p>Kesulitan melakukan pemesanan melalui Form?<br>Anda bisa menghubungi</p>
							<br>
							<div class="float-left">
								<i class="icon-placepin background-primary icon-circle-small text-size-20"></i>
							</div>
							<div class="margin-left-80 margin-bottom">
								<h4 class="text-strong margin-bottom-0">Alamat</h4>
								<p>
									Jl. Abdurahman Saleh Blok C1 No.13<br>
                                    Kel. Jurumudi  Kec. Benda
								</p>               
							</div>
							<div class="float-left">
								<i class="icon-paperplane_ico background-primary icon-circle-small text-size-20"></i>
							</div>
							<div class="margin-left-80 margin-bottom">
								<h4 class="text-strong margin-bottom-0">Email</h4>
								<p>
									<a draggable="false" href="mailto:info@dietalami.id">info@dietalami.id</a><br>
									&nbsp;
								</p>              
							</div>
							<div class="float-left">
							    <div class="background-primary icon-circle-small text-size-20" style="border-radius:100px;">
								    <img draggable="false" class="text-center" src="<?php echo"$row_setting[domain]"; ?>/images/icon-bbm.png" style="width:60px; padding:15px;">
								</div>
							</div>
							<div class="margin-left-80 margin-bottom">
								<h4 class="text-strong margin-bottom-0">BBM</h4>
								<p>
									<a draggable="false" target="_blank" href="http://pin.bbm.com/D76216A4">D76216A4</a><br>
									&nbsp;
								</p>              
							</div>
							<div class="float-left">
								<i class="fa fa-whatsapp background-primary icon-circle-small text-size-20"></i>
							</div>
							<div class="margin-left-80 margin-bottom">
								<h4 class="text-strong margin-bottom-0">Whatsapp</h4>
								<p>
									<a draggable="false" target="_blank" href="https://api.whatsapp.com/send?phone=6281517135454&text=Halo, Saya ingin memesan produk herbalnya">0815 1713 5454</a><br>
									&nbsp;
								</p>              
							</div>
							<div class="float-left">
								<i class="icon-smartphone background-primary icon-circle-small text-size-20"></i>
							</div>
							<div class="margin-left-80">
								<h4 class="text-strong margin-bottom-0">SMS</h4>
								<p>
									<a draggable="false" target="_blank" href="sms:+6281517135454?&body=Halo, Saya ingin memesan produk herbalnya">0815 1713 5454</a><br>
									&nbsp;
								</p>             
							</div>
							<div class="float-left">
								<i class="icon-smartphone background-primary icon-circle-small text-size-20"></i>
							</div>
							<div class="margin-left-80">
								<h4 class="text-strong margin-bottom-0">CALL</h4>
								<p>
									<a draggable="false" href="tel:+6281517135454">0815 1713 5454</a><br>
									&nbsp;
								</p>             
							</div>
							<br><br>
						</div>
						<div class="s-12 m-12 l-8">
							
							<h2 class="text-uppercase text-strong margin-bottom-30">Form Pemesanan - <b><b style="color:#FF0000;">Free Ongkir Jabodetabek</b></h2>
    					<?php
    					if($message_pemesanan != "" && $message_pemesanan != "sukses")
    					{
    					?>
    						<div class="alert alert-danger fade in">
    							<i class="fa fa-fw fa-warning"></i> <?php echo"$message_pemesanan"; ?>
    						</div>
    					<?
    					}
    					else if($message_pemesanan == "sukses")
    					{
    					?>
    						<div class="alert alert-success fade in">
    							<i class="fa fa-fw fa-check"></i> Terima kasih telah memesan produk kami. Kami akan menghubungi anda secepat mungkin.
    						</div>
    					<?
    					}
    					?>
							<form class="customform" name="pemesanan" action="<?php echo"$row_setting[domain]"; ?>/pemesanan" method="post">
								<div class="line">
									<div class="margin">
										<div class="s-12 m-12 l-6">
											<input type="text" name="nama_penerima_pemesanan" class="name border-radius" placeholder="Nama Penerima..." required />
										</div>
										<div class="s-12 m-12 l-6">
											<input type="text" name="telepon_pemesanan" class="name border-radius" placeholder="No Telepon / Handphone..." required />
										</div>
										<!--<div class="s-12 m-12 l-4">
											<input type="email" name="email_pemesanan" class="required email border-radius" placeholder="Email..." required />
										</div>-->
									</div>
								</div>
								<div class="s-12">
									<textarea name="alamat_pemesanan" style="resize: none;" class="required message border-radius" placeholder="Alamat..." rows="3" required></textarea>
								</div>
								<div class="line">
									<div class="margin">
										<div class="s-12 m-12 l-6">
											<input type="text" name="kota_pemesanan" class="name border-radius" placeholder="Kota..." required />
										</div>
										<div class="s-12 m-12 l-6">
											<input type="text" name="kecamatan_pemesanan" class="name border-radius" placeholder="Kecamatan..." required />
										</div>
									</div>
								</div>
								<div class="s-12">
									<select class="kurir_pemesanan form-control" id="kurir_pemesanan" name="kurir_pemesanan" data-placeholder="Pilih Kurir..." required />
										<option value="">Pilih Kurir...</option>
										<option value="JNE">JNE</option>
										<option value="TIKI">TIKI</option>
										<option value="POS">POS</option>
									</select>
								</div>
								<div class="s-12">
									<select class="jumlah_pemesanan_select form-control" id="harga_produk" name="jumlah_pemesanan_select" data-placeholder="Pilih Jumlah Pemesanan..." onChange="sum();" required />
										<option value="">Pilih Jumlah Pemesanan...</option>
										<option data-jumlah_pemesanan="1 Botol Diskon 20% - Hanya Rp 200.000" value="200000">1 Botol Diskon 20% - Hanya Rp 200.000</option>
										<option data-jumlah_pemesanan="2 Botol Diskon 34% - Hanya Rp 330.000" value="330000">2 Botol Diskon 34% - Hanya Rp 330.000</option>
									</select>
									<p>
									    Pembelian 4 Botol Free Ongkir Rp. 50.000<br>
									    Di Atas 2 Botol Langsung Hubungin Customer Service
								    </p>
								    <br>
								</div>
								<div class="s-12">
									<select class="transfer_pemesanan_select form-control" id="transfer_pemesanan_select" name="transfer_pemesanan_select" data-placeholder="Pilih Bank Transfer..." required />
										<option value="">Pilih Bank Transfer...</option>
										<option data-transfer_pemesanan="BCA 108-0090-389 A/N Liana Melani" value="BCA">BCA</option>
										<option data-transfer_pemesanan="BRI 0822-01-003155-50-7 A/N Liana Melani" value="MANDIRI">MANDIRI</option>
										<option data-transfer_pemesanan="BRI 0822-01-003155-50-7 A/N Liana Melani" value="BRI">BRI</option>
									</select>
								</div>
							    <input type="hidden" class="jumlah_pemesanan form-control" name="jumlah_pemesanan" value="" readonly />
							    <input type="hidden" class="biaya_pengiriman form-control" id="harga_ongkir" name="harga_ongkir" value="0" onselect="sum();" readonly />
								<script>
									$(".jumlah_pemesanan_select").change(function() {
										$('.jumlah_pemesanan').val($('option:selected', this).data('jumlah_pemesanan'));
										$('.harga_produk').val($('option:selected', this).data('harga_produk'));
									});
									
									$(".transfer_pemesanan_select").change(function() {
										$('.transfer_pemesanan').val($('option:selected', this).data('transfer_pemesanan'));
									});
									
									$('.transfer_pemesanan_select').change(function(){
										if($('.transfer_pemesanan_select').val() == '') {
											$('.sembunyi').hide();
										}
										else {
											$('.sembunyi').show();
										}
									});
									
									function sum() {
										var txtFirstNumberValue = document.getElementById('harga_produk').value;
										var result = parseInt(txtFirstNumberValue);
										if (!isNaN(result)) {
											document.getElementById('total_harga_pemesanan').value = result.toLocaleString();
										}
									}
								</script>
								<hr class="sembunyi">
								<div class="s-12">
                        		<?
                        		$result_nomor = mysql_query("SELECT * FROM pemesanan ORDER BY id_pemesanan DESC LIMIT 0,1");
                        		$row_nomor = mysql_fetch_array($result_nomor);
                        		$row_add_1 = substr("$row_nomor[kode_pemesanan]",3,10);
                        		$row_add = sprintf("%07d",$row_add_1+1);
                        		?>
								    <p class="sembunyi text-center" style="display:none;">
								        <b>INVOICE</b><br>
								        <br>
    									<input type="text" style="width:300px;" id="kode_pemesanan" name="kode_pemesanan" class="kode_pemesanan border-radius" placeholder="INV<?php echo"$row_add"; ?>" value="INV<?php echo"$row_add"; ?>" readonly />
    								    <br>
    								    <br>
    								    <b>TOTAL HARGA</b><br><br>
    									<input type="text" style="width:300px;" id="total_harga_pemesanan" name="total_harga_pemesanan" class="border-radius" placeholder="" value="" readonly />
    									<br>
    									<br>
									    <b>REKENING PEMBAYARAN</b><br><br>
    									<input type="text" style="width:300px;" id="transfer_pemesanan" name="transfer_pemesanan" class="transfer_pemesanan border-radius" placeholder="Rekening Pembayaran" value="" readonly />
        								<br>
        								<br>
    									KONFIRMASI PEMBAYARAN<br>
    									Format Pesanan<br>
                                        Nama # Alamat ( Kec / Kab ) # Nomor HP # Jumlah # Bca / Mandiri/ Bri<br>
                                        <br>
                                        Contoh : Lusiana Hendrika # Jl. MT Haryono No.100 Kec. Balikpapan Utara - Balikpapan # 08171234567 #  (SERBUK) # 2 Kotak # BCA
    									<br>
    								</p>
								</div>
								<hr class="sembunyi">
								<div class="sembunyi s-12 m-12 l-12  text-center" style="display:none;">
									<button class="submit-form button background-primary border-radius text-white" type="submit" name="button_submit"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i> Pesan Sekarang</button>
								</div>
							</form>
							<hr>
							<div class="s-12 m-12 l-6">
							    <h3 class="text-thin text-center text-s-size-30">
                                    <b>PENGIRIMAN</b>
                                </h3>
							    <center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/gambar-kurir.jpg" alt="Pengiriman - <?php echo"$row_setting[title]"; ?>"></center>
						        <br>
						    </div>
							<div class="s-12 m-12 l-6">
							    <h3 class="text-thin text-center text-s-size-30">
                                    <b>PEMBAYARAN</b>
                                </h3>
                                <br>
							    <center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/logo-semua-bank.png" alt="Logo Semua Bank - <?php echo"$row_setting[title]"; ?>"></center>
						        <br>
						    </div>
						</div>
					</div>
				</div>
			</div>