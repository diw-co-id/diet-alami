        <header role="banner">
			<div class="top-bar background-white">
				<div class="line">
					<div class="s-12 m-6 l-6">
						<div class="top-bar-contact">
							<p class="text-size-12"><i class="fa fa-whatsapp fa-fw" aria-hidden="true"></i> <a draggable="false" target="_blank" href="https://api.whatsapp.com/send?phone=6281517135454&text=Halo, Saya ingin memesan produk herbalnya">0815 1713 5454</a> | <i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> <a draggable="false" href="mailto:info@dietalamiid">info@dietalami.id</a></p>
						</div>
					</div>
					<div class="s-12 m-6 l-6">
						<div class="right">
							<ul class="top-bar-social right">
								<li><a draggable="false" href="<?php echo"$row_setting[facebook]"; ?>"><i class="icon-facebook_circle text-orange-hover" style="color:#3b5998;"></i></a></li>
								<li><a draggable="false" href="<?php echo"$row_setting[twitter]"; ?>"><i class="icon-twitter_circle text-orange-hover" style="color:#28a9e0;"></i></a> </li>
								<li><a draggable="false" href="<?php echo"$row_setting[google]"; ?>"><i class="icon-google_plus_circle text-orange-hover" style="color:#db5145;"></i></a></li>
								<li><a draggable="false" href="<?php echo"$row_setting[instagram]"; ?>"><i class="icon-instagram_circle text-orange-hover" style="color:#0d5784;"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
	  
			<nav class="background-white background-primary-hightlight">
				<div class="line">
					<div class="s-12 l-2">
						<a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/" class="logo"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-diet-alami.png" style="margin-top:-20px; margin-bottom:-20px;" alt="Logo <?php echo"$row_setting[name_website]"; ?> - <?php echo"$row_setting[title]"; ?>"></a>
					</div>
					<div class="top-nav s-12 l-10">
						<p class="nav-text"></p>
						<ul class="right chevron">
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Beranda</a></li>
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/bukti-pengiriman"><i class="fa fa-truck fa-fw" aria-hidden="true"></i> Bukti Pengiriman</a></li>
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/testimoni"><i class="fa fa-comment-o fa-fw" aria-hidden="true"></i> Testimoni</a></li>
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/foto-ramping-herbal"><i class="fa fa-picture-o fa-fw" aria-hidden="true"></i> Foto Ramping Herbal</a></li>
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/faq"><i class="fa fa-question-circle fa-fw" aria-hidden="true"></i> FAQ</a></li>
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/pemesanan"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i> Pemesanan</a></li>
							<!--<li><a draggable="false" href="javascript:void(0);"><i class="fa fa-newspaper-o fa-fw" aria-hidden="true"></i> Artikel</a>
								<ul>
									<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/pelangsing">Pelangsing</a></li>
									<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/diet">Diet</a></li>
								</ul>
							</li>-->
							<li><a draggable="false" href="<?php echo"$row_setting[domain]"; ?>/hubungi-kami"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> Hubungi Kami</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>