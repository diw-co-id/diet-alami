<?php
include("connect_server.php");

if($_COOKIE['id_admin'] != 0)
{
	if(isset($_POST['button_submit']))
	{
		$name_website = $_POST['name_website'];
		$domain = $_POST['domain'];
		$domain_admin = $_POST['domain_admin'];
		$refresh = $_POST['refresh'];
		$logo = $_POST['logo'];
		$favicon = $_POST['favicon'];
		$created_by = $_POST['created_by'];
		$name = $_POST['name'];
		$title = $_POST['title'];
		$description = $_POST['description'];
		$keywords = $_POST['keywords'];
		$subject = $_POST['subject'];
		$abstract = $_POST['abstract'];
		$application_name = $_POST['application_name'];
		$designer = $_POST['designer'];
		$creator = $_POST['creator'];
		$publisher = $_POST['publisher'];
		$author = $_POST['author'];
		$rights = $_POST['rights'];
		$referrer = $_POST['referrer'];
		$language = $_POST['language'];
		$aesop = $_POST['aesop'];
		$target = $_POST['target'];
		$revisit = $_POST['revisit'];
		$revisit_after = $_POST['revisit_after'];
		$formatter = $_POST['formatter'];
		$generator = $_POST['generator'];
		$copyright = $_POST['copyright'];
		$itemprop_name = $_POST['itemprop_name'];
		$itemprop_description = $_POST['itemprop_description'];
		$itemprop_image = $_POST['itemprop_image'];
		$alexa_verify_id = $_POST['alexa_verify_id'];
		$google_site_verification = $_POST['google_site_verification'];
		$google_analytics_id = $_POST['google_analytics_id'];
		$p_domain_verify = $_POST['p_domain_verify'];
		$msvalidate_01 = $_POST['msvalidate_01'];
		$norton_safeweb_site_verification = $_POST['norton_safeweb_site_verification'];
		$wot_verification = $_POST['wot_verification'];
		$avgthreatlabs_verification = $_POST['avgthreatlabs_verification'];
		$article_publisher = $_POST['article_publisher'];
		$og_type = $_POST['og_type'];
		$og_url = $_POST['og_url'];
		$og_title = $_POST['og_title'];
		$og_site_name = $_POST['og_site_name'];
		$og_description = $_POST['og_description'];
		$og_image = $_POST['og_image'];
		$dc_title = $_POST['dc_title'];
		$dc_creator = $_POST['dc_creator'];
		$dc_publisher = $_POST['dc_publisher'];
		$dc_type = $_POST['dc_type'];
		$dc_format = $_POST['dc_format'];
		$dc_identifier = $_POST['dc_identifier'];
		$fb_pages = $_POST['fb_pages'];
		$fb_app_id = $_POST['fb_app_id'];
		$fb_page_id = $_POST['fb_page_id'];
		$fb_admins = $_POST['fb_admins'];
		$twitter_account_id = $_POST['twitter_account_id'];
		$twitter_card = $_POST['twitter_card'];
		$twitter_app_id_ipad = $_POST['twitter_app_id_ipad'];
		$twitter_app_id_iphone = $_POST['twitter_app_id_iphone'];
		$twitter_app_name_ipad = $_POST['twitter_app_name_ipad'];
		$twitter_app_name_iphone = $_POST['twitter_app_name_iphone'];
		$twitter_site = $_POST['twitter_site'];
		$twitter_creator = $_POST['twitter_creator'];
		$twitter_title = $_POST['twitter_title'];
		$twitter_description = $_POST['twitter_description'];
		$twitter_image = $_POST['twitter_image'];
		$twitter_image_src = $_POST['twitter_image_src'];
		$geo_region = $_POST['geo_region'];
		$geo_placename = $_POST['geo_placename'];
		$geo_country = $_POST['geo_country'];
		$geo_position = $_POST['geo_position'];
		$distribution = $_POST['distribution'];
		$icbm = $_POST['icbm'];
		$openid_server = $_POST['openid_server'];
		$openid_delegate = $_POST['openid_delegate'];
		$link_author = $_POST['link_author'];
		$link_publisher = $_POST['link_publisher'];
		$facebook = $_POST['facebook'];
		$twitter = $_POST['twitter'];
		$google = $_POST['google'];
		$instagram = $_POST['instagram'];
		$youtube = $_POST['youtube'];
		$tumblr = $_POST['tumblr'];
		$pinterest = $_POST['pinterest'];
		$linkedin = $_POST['linkedin'];
		$kurs_dollar = $_POST['kurs_dollar'];
	
		if (!empty($_FILES["logo"]["tmp_name"]))
		{
			$file_size = $_FILES['logo']['size'];
			$jenis_gambar = $_FILES['logo']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 1000000))
				{
					$message_pengaturan = "Ukuran Gambar Maksimal 1mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['logo']['name']));
					if (move_uploaded_file($_FILES['logo']['tmp_name'], '/home/anuf9542/public_html/kangaroo/images'.$gambar))
					{
						mysql_query("UPDATE setting SET logo = '$gambar', name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
						$message_pengaturan = "sukses";
					}
					else
					{
						$message_pengaturan = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_pengaturan = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			mysql_query("UPDATE setting SET name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
			$message_pengaturan = "sukses";
		}
	
		if (!empty($_FILES["favicon"]["tmp_name"]))
		{
			$file_size = $_FILES['favicon']['size'];
			$jenis_gambar = $_FILES['favicon']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 1000000))
				{
					$message_pengaturan = "Ukuran Gambar Maksimal 1mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['favicon']['name']));
					if (move_uploaded_file($_FILES['favicon']['tmp_name'], '/home/anuf9542/public_html/kangaroo/images'.$gambar))
					{
						mysql_query("UPDATE setting SET favicon = '$gambar', name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
						$message_pengaturan = "sukses";
					}
					else
					{
						$message_pengaturan = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_pengaturan = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			mysql_query("UPDATE setting SET name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
			$message_pengaturan = "sukses";
		}
	
		if (!empty($_FILES["itemprop_image"]["tmp_name"]))
		{
			$file_size = $_FILES['itemprop_image']['size'];
			$jenis_gambar = $_FILES['itemprop_image']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 1000000))
				{
					$message_pengaturan = "Ukuran Gambar Maksimal 1mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['itemprop_image']['name']));
					if (move_uploaded_file($_FILES['itemprop_image']['tmp_name'], '/home/anuf9542/public_html/kangaroo/images'.$gambar))
					{
						mysql_query("UPDATE setting SET itemprop_image = '$gambar', name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
						$message_pengaturan = "sukses";
					}
					else
					{
						$message_pengaturan = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_pengaturan = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			mysql_query("UPDATE setting SET name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
			$message_pengaturan = "sukses";
		}
	
		if (!empty($_FILES["og_image"]["tmp_name"]))
		{
			$file_size = $_FILES['og_image']['size'];
			$jenis_gambar = $_FILES['og_image']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 1000000))
				{
					$message_pengaturan = "Ukuran Gambar Maksimal 1mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['og_image']['name']));
					if (move_uploaded_file($_FILES['og_image']['tmp_name'], '/home/anuf9542/public_html/kangaroo/images'.$gambar))
					{
						mysql_query("UPDATE setting SET og_image = '$gambar', name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
						$message_pengaturan = "sukses";
					}
					else
					{
						$message_pengaturan = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_pengaturan = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			mysql_query("UPDATE setting SET name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
			$message_pengaturan = "sukses";
		}
	
		if (!empty($_FILES["twitter_image"]["tmp_name"]))
		{
			$file_size = $_FILES['twitter_image']['size'];
			$jenis_gambar = $_FILES['twitter_image']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 1000000))
				{
					$message_pengaturan = "Ukuran Gambar Maksimal 1mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['twitter_image']['name']));
					if (move_uploaded_file($_FILES['twitter_image']['tmp_name'], '/home/anuf9542/public_html/kangaroo/images'.$gambar))
					{
						mysql_query("UPDATE setting SET twitter_image = '$gambar', name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
						$message_pengaturan = "sukses";
					}
					else
					{
						$message_pengaturan = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_pengaturan = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			mysql_query("UPDATE setting SET name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
			$message_pengaturan = "sukses";
		}
	
		if (!empty($_FILES["twitter_image_src"]["tmp_name"]))
		{
			$file_size = $_FILES['twitter_image_src']['size'];
			$jenis_gambar = $_FILES['twitter_image_src']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 1000000))
				{
					$message_pengaturan = "Ukuran Gambar Maksimal 1mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['twitter_image_src']['name']));
					if (move_uploaded_file($_FILES['twitter_image_src']['tmp_name'], '/home/anuf9542/public_html/kangaroo/images'.$gambar))
					{
						mysql_query("UPDATE setting SET twitter_image_src = '$gambar', name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
						$message_pengaturan = "sukses";
					}
					else
					{
						$message_pengaturan = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_pengaturan = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			mysql_query("UPDATE setting SET name_website = '$name_website', domain = '$domain', domain_admin = '$domain_admin', refresh = '$refresh', created_by = '$created_by', name = '$name', title = '$title', description = '$description', keywords = '$keywords', subject = '$subject', abstract = '$abstract', application_name = '$application_name', designer = '$designer', creator = '$creator', publisher = '$publisher', author = '$author', rights = '$rights', referrer = '$referrer', language = '$language', aesop = '$aesop', target = '$target', revisit = '$revisit', revisit_after = '$revisit_after', formatter = '$formatter', generator = '$generator', copyright = '$copyright', itemprop_name = '$itemprop_name', itemprop_description = '$itemprop_description', alexa_verify_id = '$alexa_verify_id', google_site_verification = '$google_site_verification', google_analytics_id = '$google_analytics_id', p_domain_verify = '$p_domain_verify', msvalidate_01 = '$msvalidate_01', norton_safeweb_site_verification = '$norton_safeweb_site_verification', wot_verification = '$wot_verification', avgthreatlabs_verification = '$avgthreatlabs_verification', article_publisher = '$article_publisher', og_type = '$og_type', og_url = '$og_url', og_title = '$og_title', og_site_name = '$og_site_name', og_description = '$og_description', dc_title = '$dc_title', dc_creator = '$dc_creator', dc_publisher = '$dc_publisher', dc_type = '$dc_type', dc_format = '$dc_format', dc_identifier = '$dc_identifier', fb_pages = '$fb_pages', fb_app_id = '$fb_app_id', fb_page_id = '$fb_page_id', fb_admins = '$fb_admins', twitter_account_id = '$twitter_account_id', twitter_card = '$twitter_card', twitter_app_id_ipad = '$twitter_app_id_ipad', twitter_app_id_iphone = '$twitter_app_id_iphone', twitter_app_name_ipad = '$twitter_app_name_ipad', twitter_app_name_iphone = '$twitter_app_name_iphone', twitter_site = '$twitter_site', twitter_creator = '$twitter_creator', twitter_title = '$twitter_title', twitter_description = '$twitter_description', geo_region = '$geo_region', geo_placename = '$geo_placename', geo_country = '$geo_country', geo_position = '$geo_position', distribution = '$distribution', icbm = '$icbm', openid_server = '$openid_server', openid_delegate = '$openid_delegate', link_author = '$link_author', link_publisher = '$link_publisher', facebook = '$facebook', twitter = '$twitter', google = '$google', instagram = '$instagram', youtube = '$youtube', tumblr = '$tumblr', pinterest = '$pinterest', linkedin = '$linkedin', kurs_dollar = '$kurs_dollar' WHERE id_setting = '1'");
							
			$message_pengaturan = "sukses";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $judul = 'Pengaturan (Admin Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li class="active"><i class="ace-icon fa fa-cog home-icon fa-fw"></i> Pengaturan</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-cog home-icon fa-fw"></i> Pengaturan</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="alert alert-block alert-info">
									<i class="ace-icon fa fa-warning red fa-fw"></i>

									<strong class="red">Peringatan!</strong> Pengaturan ini akan mempengaruhi sistem website anda, mohon untuk mengikuti petunjuk yang ada. Jika terdapat kesulitan, hubungi pihak <a draggable="false" href="https://www.diw.co.id" target="_blank"><strong class="blue">DIW.co.id (Develop Inspire Website)</strong></a>
								</div>
							<?php
							if($message_pengaturan != "" && $message_pengaturan != "sukses")
							{
							?>
								<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-warning"></i> <?php echo"$message_pengaturan"; ?>
								</div>
							<?
							}
							else if($message_pengaturan == "sukses")
							{
							?>
								<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-check"></i> Berhasil, pengaturan telah diubah.
								</div>
							<?
							}
							?>
								<form class="form-horizontal" role="form" name="pengaturan" action="<?php echo"$row_setting[domain_admin]"; ?>/pengaturan" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="name_website">Name Website</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="name_website" name="name_website" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[name_website]"; ?>" value="<?php echo"$row_setting[name_website]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="domain">Domain</label>

										<div class="col-sm-6">
											<input type="url" class="form-control" id="domain" name="domain" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100, Must Link" placeholder="<?php echo"$row_setting[domain]"; ?>" value="<?php echo"$row_setting[domain]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="domain_admin">Domain Admin</label>

										<div class="col-sm-6">
											<input type="url" class="form-control" id="domain_admin" name="domain_admin" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100, Must Link" placeholder="<?php echo"$row_setting[domain_admin]"; ?>" value="<?php echo"$row_setting[domain_admin]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="refresh">Refresh</label>

										<div class="col-sm-6">
											<input type="text" pattern="[0-9]*" min="1" max="9999999999" class="form-control" id="spinner-1" name="refresh" maxlength="10" data-rel="tooltip" data-placement="top" title="Max Char 10, Numeric" placeholder="<?php echo"$row_setting[refresh]"; ?>" value="<?php echo"$row_setting[refresh]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Logo</label>

										<div class="col-sm-3">
										<?
										if($row_setting[logo] == "")
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/no-image-available.png" width="100%"><hr>
										<?
										}
										else
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[logo]"; ?>" width="100%"><hr>
										<?
										}
										?>
											<input type="file" class="form-control" id="id-input-file-2" name="logo" maxlength="100" data-rel="tooltip" data-placement="top" title="Leave Blank If You Not Change. Must Picture, Format .PNG .JPG .JPEG, Max Size 1mb" placeholder="<?php echo"$row_setting[logo]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Favicon</label>

										<div class="col-sm-3">
										<?
										if($row_setting[favicon] == "")
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/no-image-available.png" width="100%"><hr>
										<?
										}
										else
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[favicon]"; ?>" width="100%"><hr>
										<?
										}
										?>
											<input type="file" class="form-control" id="id-input-file-2" name="favicon" maxlength="100" data-rel="tooltip" data-placement="top" title="Leave Blank If You Not Change. Must Picture, Format .PNG .JPG .JPEG, Max Size 1mb" placeholder="<?php echo"$row_setting[favicon]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="created_by">Created By</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="created_by" name="created_by" class="autosize-transition form-control" maxlength="5000" data-rel="tooltip" data-placement="top" title="Max Char 5000" placeholder="<?php echo"$row_setting[created_by]"; ?>" /><?php echo"$row_setting[created_by]"; ?></textarea>

										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="name">Name</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="name" name="name" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[name]"; ?>" value="<?php echo"$row_setting[name]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="title">Title</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="title" name="title" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[title]"; ?>" value="<?php echo"$row_setting[title]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="description">Description</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="description" name="description" class="autosize-transition form-control" maxlength="160" data-rel="tooltip" data-placement="top" title="Max Char 160" placeholder="<?php echo"$row_setting[description]"; ?>" /><?php echo"$row_setting[description]"; ?></textarea>

										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="keywords">Keywords</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="keywords" name="keywords" class="autosize-transition form-control" maxlength="1000" data-rel="tooltip" data-placement="top" title="Max Char 1000" placeholder="<?php echo"$row_setting[keywords]"; ?>" /><?php echo"$row_setting[keywords]"; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="subject">Subject</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="subject" name="subject" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[title]"; ?>" value="<?php echo"$row_setting[subject]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="abstract">Abstract</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="abstract" name="abstract" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[abstract]"; ?>" value="<?php echo"$row_setting[abstract]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="application_name">Application Name</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="application_name" name="application_name" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[application_name]"; ?>" value="<?php echo"$row_setting[application_name]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="designer">Designer</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="designer" name="designer" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[designer]"; ?>" value="<?php echo"$row_setting[designer]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="creator">Creator</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="creator" name="creator" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[creator]"; ?>" value="<?php echo"$row_setting[creator]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="publisher">Publisher</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="publisher" name="publisher" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[publisher]"; ?>" value="<?php echo"$row_setting[publisher]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="author">Author</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="author" name="author" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[author]"; ?>" value="<?php echo"$row_setting[author]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="rights">Rights</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="rights" name="rights" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[rights]"; ?>" value="<?php echo"$row_setting[rights]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="referrer">Referrer</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="referrer" name="referrer" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[referrer]"; ?>" value="<?php echo"$row_setting[referrer]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="language">Language</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="language" name="language" maxlength="2" data-rel="tooltip" data-placement="top" title="Max Char 2" placeholder="<?php echo"$row_setting[language]"; ?>" value="<?php echo"$row_setting[language]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="aesop">Aesop</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="aesop" name="aesop" maxlength="20" data-rel="tooltip" data-placement="top" title="Max Char 20" placeholder="<?php echo"$row_setting[aesop]"; ?>" value="<?php echo"$row_setting[aesop]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="target">Target</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="target" name="target" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 10" placeholder="<?php echo"$row_setting[target]"; ?>" value="<?php echo"$row_setting[target]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="revisit">Revisit</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="revisit" name="revisit" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 10" placeholder="<?php echo"$row_setting[revisit]"; ?>" value="<?php echo"$row_setting[revisit]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="revisit_after">Revisit After</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="spinner-2" name="revisit_after" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 10, Numeric" placeholder="<?php echo"$row_setting[revisit_after]"; ?>" value="<?php echo"$row_setting[revisit_after]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="formatter">Formatter</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="formatter" name="formatter" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[formatter]"; ?>" value="<?php echo"$row_setting[formatter]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="generator">Referrer</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="generator" name="generator" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[generator]"; ?>" value="<?php echo"$row_setting[generator]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="copyright">Copyright</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="copyright" name="copyright" class="autosize-transition form-control" maxlength="200" data-rel="tooltip" data-placement="top" title="Max Char 200" placeholder="<?php echo"$row_setting[copyright]"; ?>" /><?php echo"$row_setting[copyright]"; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="itemprop_name">Itemprop Name</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="itemprop_name" name="itemprop_name" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[itemprop_name]"; ?>" value="<?php echo"$row_setting[itemprop_name]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="itemprop_description">Itemprop Description</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="itemprop_description" name="itemprop_description" class="autosize-transition form-control" maxlength="200" data-rel="tooltip" data-placement="top" title="Max Char 200" placeholder="<?php echo"$row_setting[itemprop_description]"; ?>" /><?php echo"$row_setting[itemprop_description]"; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Itemprop Image</label>

										<div class="col-sm-3">
										<?
										if($row_setting[itemprop_image] == "")
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/no-image-available.png" width="100%"><hr>
										<?
										}
										else
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[itemprop_image]"; ?>" width="100%"><hr>
										<?
										}
										?>
											<input type="file" class="form-control" id="id-input-file-2" name="itemprop_image" maxlength="100" data-rel="tooltip" data-placement="top" title="Leave Blank If You Not Change. Must Picture, Format .PNG .JPG .JPEG, Max Size 1mb" placeholder="<?php echo"$row_setting[itemprop_image]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="alexa_verify_id">Alexa Verify ID</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="alexa_verify_id" name="alexa_verify_id" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[alexa_verify_id]"; ?>" value="<?php echo"$row_setting[alexa_verify_id]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="google_site_verification">Google Site Verification</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="google_site_verification" name="google_site_verification" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[google_site_verification]"; ?>" value="<?php echo"$row_setting[google_site_verification]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="google_analytics_id">Google Analytics ID</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="google_analytics_id" name="google_analytics_id" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[google_analytics_id]"; ?>" value="<?php echo"$row_setting[google_analytics_id]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="p_domain_verify">P Domain Verify</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="p_domain_verify" name="p_domain_verify" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[p_domain_verify]"; ?>" value="<?php echo"$row_setting[p_domain_verify]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="msvalidate_01">MsValidate 01</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="msvalidate_01" name="msvalidate_01" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[msvalidate_01]"; ?>" value="<?php echo"$row_setting[msvalidate_01]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="norton_safeweb_site_verification">Norton SafeWeb Site Verification</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="norton_safeweb_site_verification" name="norton_safeweb_site_verification" maxlength="200" data-rel="tooltip" data-placement="top" title="Max Char 200" placeholder="<?php echo"$row_setting[norton_safeweb_site_verification]"; ?>" value="<?php echo"$row_setting[norton_safeweb_site_verification]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="wot_verification">WOT Verification</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="wot_verification" name="wot_verification" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[wot_verification]"; ?>" value="<?php echo"$row_setting[wot_verification]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="avgthreatlabs_verification">AVG ThreatLabs Verification</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="avgthreatlabs_verification" name="avgthreatlabs_verification" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[avgthreatlabs_verification]"; ?>" value="<?php echo"$row_setting[avgthreatlabs_verification]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="article_publisher">Article Publisher</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="article_publisher" name="article_publisher" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[article_publisher]"; ?>" value="<?php echo"$row_setting[article_publisher]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="og_type">OG Type</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="og_type" name="og_type" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[og_type]"; ?>" value="<?php echo"$row_setting[og_type]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="og_url">OG URL</label>

										<div class="col-sm-6">
											<input type="url" class="form-control" id="og_url" name="og_url" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[og_url]"; ?>" value="<?php echo"$row_setting[og_url]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="og_title">OG Title</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="og_title" name="og_title" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[og_title]"; ?>" value="<?php echo"$row_setting[og_title]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="og_site_name">OG Site Name</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="og_site_name" name="og_site_name" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[og_site_name]"; ?>" value="<?php echo"$row_setting[og_site_name]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="og_description">OG Description</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="og_description" name="og_description" class="autosize-transition form-control" maxlength="200" data-rel="tooltip" data-placement="top" title="Max Char 200" placeholder="<?php echo"$row_setting[og_description]"; ?>" /><?php echo"$row_setting[og_description]"; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">OG Image</label>

										<div class="col-sm-3">
										<?
										if($row_setting[og_image] == "")
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/no-image-available.png" width="100%"><hr>
										<?
										}
										else
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[og_image]"; ?>" width="100%"><hr>
										<?
										}
										?>
											<input type="file" class="form-control" id="id-input-file-2" name="og_image" maxlength="100" data-rel="tooltip" data-placement="top" title="Leave Blank If You Not Change. Must Picture, Format .PNG .JPG .JPEG, Max Size 1mb" placeholder="<?php echo"$row_setting[og_image]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="dc_title">DC Title</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="dc_title" name="dc_title" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[dc_title]"; ?>" value="<?php echo"$row_setting[dc_title]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="dc_creator">DC Creator</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="dc_creator" name="dc_creator" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[dc_creator]"; ?>" value="<?php echo"$row_setting[dc_creator]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="dc_publisher">DC Publisher</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="dc_publisher" name="dc_publisher" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[dc_publisher]"; ?>" value="<?php echo"$row_setting[dc_publisher]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="dc_type">DC Type</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="dc_type" name="dc_type" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[dc_type]"; ?>" value="<?php echo"$row_setting[dc_type]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="dc_format">DC Format</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="dc_format" name="dc_format" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[dc_format]"; ?>" value="<?php echo"$row_setting[dc_format]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="dc_identifier">DC Identifier</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="dc_identifier" name="dc_identifier" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[dc_identifier]"; ?>" value="<?php echo"$row_setting[dc_identifier]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="fb_pages">FB Pages</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="fb_pages" name="fb_pages" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[fb_pages]"; ?>" value="<?php echo"$row_setting[fb_pages]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="fb_app_id">FB App ID</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="fb_app_id" name="fb_app_id" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[fb_app_id]"; ?>" value="<?php echo"$row_setting[fb_app_id]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="fb_page_id">FB Page ID</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="fb_page_id" name="fb_page_id" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[fb_page_id]"; ?>" value="<?php echo"$row_setting[fb_page_id]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="fb_admins">FB Admins</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="fb_admins" name="fb_admins" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[fb_admins]"; ?>" value="<?php echo"$row_setting[fb_admins]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_account_id">Twitter Account ID</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_account_id" name="twitter_account_id" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_account_id]"; ?>" value="<?php echo"$row_setting[twitter_account_id]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_card">Twitter Card</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_card" name="twitter_card" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_card]"; ?>" value="<?php echo"$row_setting[twitter_card]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_app_id_ipad">Twitter App ID Ipad</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_app_id_ipad" name="twitter_app_id_ipad" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_app_id_ipad]"; ?>" value="<?php echo"$row_setting[twitter_app_id_ipad]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_app_id_iphone">Twitter App ID Iphone</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_app_id_iphone" name="twitter_app_id_iphone" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_app_id_iphone]"; ?>" value="<?php echo"$row_setting[twitter_app_id_iphone]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_app_name_ipad">Twitter App Name Ipad</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_app_name_ipad" name="twitter_app_name_ipad" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_app_name_ipad]"; ?>" value="<?php echo"$row_setting[twitter_app_name_ipad]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_app_name_iphone">Twitter App Name Iphone</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_app_name_iphone" name="twitter_app_name_iphone" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_app_name_iphone]"; ?>" value="<?php echo"$row_setting[twitter_app_name_iphone]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_site">Twitter Site</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_site" name="twitter_site" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_site]"; ?>" value="<?php echo"$row_setting[twitter_site]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_creator">Twitter Creator</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_creator" name="twitter_creator" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter_creator]"; ?>" value="<?php echo"$row_setting[twitter_creator]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_title">Twitter Title</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter_title" name="twitter_title" maxlength="60" data-rel="tooltip" data-placement="top" title="Max Char 60" placeholder="<?php echo"$row_setting[twitter_title]"; ?>" value="<?php echo"$row_setting[twitter_title]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter_description">Twitter Description</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="twitter_description" name="twitter_description" class="autosize-transition form-control" maxlength="200" data-rel="tooltip" data-placement="top" title="Max Char 200" placeholder="<?php echo"$row_setting[twitter_description]"; ?>" /><?php echo"$row_setting[twitter_description]"; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Twitter Image</label>

										<div class="col-sm-3">
										<?
										if($row_setting[twitter_image] == "")
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/no-image-available.png" width="100%"><hr>
										<?
										}
										else
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[twitter_image]"; ?>" width="100%"><hr>
										<?
										}
										?>
											<input type="file" class="form-control" id="id-input-file-2" name="twitter_image" maxlength="100" data-rel="tooltip" data-placement="top" title="Leave Blank If You Not Change. Must Picture, Format .PNG .JPG .JPEG, Max Size 1mb" placeholder="<?php echo"$row_setting[twitter_image]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Twitter Image Src</label>

										<div class="col-sm-3">
										<?
										if($row_setting[twitter_image_src] == "")
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/no-image-available.png" width="100%"><hr>
										<?
										}
										else
										{
										?>
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/<?php echo"$row_setting[twitter_image_src]"; ?>" width="100%"><hr>
										<?
										}
										?>
											<input type="file" class="form-control" id="id-input-file-2" name="twitter_image_src" maxlength="100" data-rel="tooltip" data-placement="top" title="Leave Blank If You Not Change. Must Picture, Format .PNG .JPG .JPEG, Max Size 1mb" placeholder="<?php echo"$row_setting[twitter_image_src]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="geo_region">Geo Region</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="geo_region" name="geo_region" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[geo_region]"; ?>" value="<?php echo"$row_setting[geo_region]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="geo_placename">Geo Placename</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="geo_placename" name="geo_placename" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[geo_placename]"; ?>" value="<?php echo"$row_setting[geo_placename]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="geo_country">Geo Country</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="geo_country" name="geo_country" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[geo_country]"; ?>" value="<?php echo"$row_setting[geo_country]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="geo_position">Geo Position</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="geo_position" name="geo_position" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[geo_position]"; ?>" value="<?php echo"$row_setting[geo_position]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="distribution">Distribution</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="distribution" name="distribution" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[distribution]"; ?>" value="<?php echo"$row_setting[distribution]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="icbm">ICBM</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="icbm" name="icbm" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[icbm]"; ?>" value="<?php echo"$row_setting[icbm]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="openid_server">OpenID Server</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="openid_server" name="openid_server" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[openid_server]"; ?>" value="<?php echo"$row_setting[openid_server]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="openid_delegate">OpenID Delegate</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="openid_delegate" name="openid_delegate" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[openid_delegate]"; ?>" value="<?php echo"$row_setting[openid_delegate]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="link_author">Link Author</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="link_author" name="link_author" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[link_author]"; ?>" value="<?php echo"$row_setting[link_author]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="link_publisher">Link Publisher</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="link_publisher" name="link_publisher" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[link_publisher]"; ?>" value="<?php echo"$row_setting[link_publisher]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="facebook">Facebook</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="facebook" name="facebook" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[facebook]"; ?>" value="<?php echo"$row_setting[facebook]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="twitter">Twitter</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="twitter" name="twitter" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[twitter]"; ?>" value="<?php echo"$row_setting[twitter]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="google">Google</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="google" name="google" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[google]"; ?>" value="<?php echo"$row_setting[google]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="instagram">Instagram</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="instagram" name="instagram" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[instagram]"; ?>" value="<?php echo"$row_setting[instagram]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="youtube">Youtube</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="youtube" name="youtube" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[youtube]"; ?>" value="<?php echo"$row_setting[youtube]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="tumblr">Tumblr</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="tumblr" name="tumblr" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[tumblr]"; ?>" value="<?php echo"$row_setting[tumblr]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="pinterest">Pinterest</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="pinterest" name="pinterest" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[pinterest]"; ?>" value="<?php echo"$row_setting[pinterest]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="linkedin">Linkedin</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="linkedin" name="linkedin" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 100" placeholder="<?php echo"$row_setting[linkedin]"; ?>" value="<?php echo"$row_setting[linkedin]"; ?>" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="kurs_dollar">Kurs Dollar</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="spinner-3" name="kurs_dollar" maxlength="100" data-rel="tooltip" data-placement="top" title="Max Char 10, Numeric" placeholder="<?php echo"$row_setting[kurs_dollar]"; ?>" value="<?php echo"$row_setting[kurs_dollar]"; ?>" />
										</div>
									</div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="button_submit" type="submit">
												<i class="ace-icon fa fa-check bigger-110 fa-fw"></i>
												Simpan Perubahan
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110 fa-fw"></i>
												Reset
											</button>
										</div>
									</div>
								</form><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>