<?php
include("connect_server.php");

if($_COOKIE['id_admin'] != 0)
{
	if(isset($_POST['button_submit']))
	{
		$pertanyaan_faq_escape_string = $_POST['pertanyaan_faq'];
		$pertanyaan_faq = mysql_escape_string($pertanyaan_faq_escape_string);
		
		$jawaban_faq_escape_string = $_POST['jawaban_faq'];
		$jawaban_faq = mysql_escape_string($jawaban_faq_escape_string);
	
		if($pertanyaan_faq_escape_string != '' || $jawaban_faq_escape_string != '')
		{
			mysql_query("INSERT INTO faq (pertanyaan_faq, jawaban_faq) VALUES ('$pertanyaan_faq', '$jawaban_faq')");
				
			$message_tambah_faq = "sukses";
			?>
			<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/faq"</script>
			<?
		}
		else
		{
			$message_tambah_faq = "Mohon isi data yang kosong.";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $pertanyaan = 'Tambah FAQ (FAQ Panel)'; ?>

		<title><?php echo"$pertanyaan"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li><i class="ace-icon fa fa-question-circle home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/faq">FAQ</a></li>
							<li class="active"><i class="ace-icon fa fa-plus home-icon fa-fw"></i> Tambah FAQ</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-plus home-icon fa-fw"></i> Tambah FAQ</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							<?php
							if($message_tambah_faq != "" && $message_tambah_faq != "sukses")
							{
							?>
								<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-warning"></i> <?php echo"$message_tambah_faq"; ?>
								</div>
							<?
							}
							else if($message_tambah_faq == "sukses")
							{
							?>
								<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-check"></i> Berhasil, faq telah ditambah.
								</div>
							<?
							}
							?>
								<form class="form-horizontal" role="form" name="tambah_faq" action="<?php echo"$row_setting[domain_admin]"; ?>/tambah_faq" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="pertanyaan_faq">Pertanyaan FAQ</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="pertanyaan_faq" name="pertanyaan_faq" class="autosize-transition form-control" maxlength="1000" data-rel="tooltip" data-placement="top" title="Max Char 1000" placeholder="Masukan Pertanyaan FAQ..." required /></textarea>

										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="jawaban_faq">Jawaban FAQ</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="jawaban_faq" name="jawaban_faq" class="autosize-transition form-control" maxlength="1000" data-rel="tooltip" data-placement="top" title="Max Char 1000" placeholder="Masukan Jawaban FAQ..." required /></textarea>

										</div>
									</div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="button_submit" type="submit">
												<i class="ace-icon fa fa-check bigger-110 fa-fw"></i>
												Tambah FAQ
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110 fa-fw"></i>
												Reset
											</button>
										</div>
									</div>
								</form><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>