<?php
include("connect_server.php");

if($_COOKIE['id_admin'] != 0)
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $judul = 'Admin (Admin Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li class="active"><i class="ace-icon fa fa-user home-icon fa-fw"></i> Admin</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-user home-icon fa-fw"></i> Admin</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<div class="row">
									<div class="col-xs-12">

										<div class="clearfix">
											<div class="pull-left">
												<button onClick="window.location='<?php echo"$row_setting[domain_admin]"; ?>/tambah_admin'" class="btn btn-white btn-info btn-bold">
													<i class="ace-icon fa fa-plus bigger-120 blue"></i>
													Tambah Admin
												</button>
											</div>
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header">
											Total : <b><?php echo"$row_total_admin"; ?></b> Admin
										</div>

										<!-- div.dataTables_borderWrap -->
										<div>

											<table id="dynamic-table" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>No</th>
														<th style="width:15%"><i class="ace-icon fa fa-picture-o bigger-110 hidden-480 fa-fw"></i> Foto Admin</th>
														<th><i class="ace-icon fa fa-user bigger-110 hidden-480 fa-fw"></i> Username Admin</th>
														<th><i class="ace-icon fa fa-user bigger-110 hidden-480 fa-fw"></i> Nama Admin</th>
														<th style="display:none;"></th>
														<th style="display:none;"></th>
														<th><i class="ace-icon fa fa-wrench bigger-110 hidden-480 fa-fw"></i> Aksi</th>
													</tr>
												</thead>

												<tbody>
												<?php
												$a=0;
												$result_data_admin = mysql_query("SELECT * FROM admin ORDER BY id_admin DESC");
												while($row_data_admin = mysql_fetch_array($result_data_admin))
												{
													$a++;
												?>
													<tr>
														<td><?php echo"$a"; ?></td>
														<td><img draggable="false" src="<?php echo"$row_setting[domain_admin]"; ?>/images/admin/<?php echo"$row_data_admin[foto_admin]"; ?>" width="100%"></td>
														<td><?php echo"$row_data_admin[username_admin]"; ?></td>
														<td><?php echo"$row_data_admin[nama_admin]"; ?></td>
														<td style="display:none;"></td>
														<td style="display:none;"></td>
														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a draggable="false" class="green" href="<?php echo"$row_setting[domain_admin]"; ?>/ubah_admin/<?php echo"$row_data_admin[id_admin]"; ?>" data-rel="tooltip" data-placement="top" title="Ubah Admin - <?php echo"$row_data_admin[username_admin]"; ?>">
																	<i class="ace-icon fa fa-edit bigger-130"></i>
																</a>
																<a draggable="false" class="red" href="#hapus_admin_<?php echo"$row_data_admin[id_admin]"; ?>" role="button" data-toggle="modal" data-rel="tooltip" data-placement="top" title="Hapus Admin - <?php echo"$row_data_admin[username_admin]"; ?>">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</div>

															<div class="hidden-md hidden-lg">
																<div class="inline pos-rel">
																	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
																		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
																	</button>

																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

																		<li>
																			<a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/ubah_admin/<?php echo"$row_data_admin[id_admin]"; ?>" class="tooltip-success" data-rel="tooltip" title="Ubah Admin - <?php echo"$row_data_admin[username_admin]"; ?>">
																				<span class="green">
																					<i class="ace-icon fa fa-edit bigger-120"></i>
																				</span>
																			</a>
																		</li>

																		<li>
																			<a draggable="false" href="#hapus_admin_<?php echo"$row_data_admin[id_admin]"; ?>" role="button" data-toggle="modal" class="tooltip-error" data-rel="tooltip" title="Hapus Admin - <?php echo"$row_data_admin[username_admin]"; ?>">
																				<span class="red">
																					<i class="ace-icon fa fa-trash-o bigger-120"></i>
																				</span>
																			</a>
																		</li>
																	</ul>
																</div>
															</div>
														</td>
													</tr>

													<div id="hapus_admin_<?php echo"$row_data_admin[id_admin]"; ?>" class="modal fade" tabindex="-1">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header no-padding">
																	<div class="table-header">
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																			<span class="white">&times;</span>
																		</button>
																		<i class="ace-icon fa fa-warning"></i> Informasi
																	</div>
																</div>

																<div class="modal-body">
																	<div class="row">
																		<div class="col-xs-12">
																			<h4>Anda yakin untuk menghapus admin <b>"<?php echo"$row_data_admin[username_admin]"; ?>"</b></h4>
																		</div>
																	</div>
																</div>

																<div class="modal-footer no-margin-top">
																	<button class="btn btn-sm btn-default" data-dismiss="modal">
																		<i class="ace-icon fa fa-times"></i>
																		Tidak
																	</button>
																	<button class="btn btn-sm btn-danger" onClick="window.location='<?php echo"$row_setting[domain_admin]"; ?>/hapus_admin/<?php echo"$row_data_admin[id_admin]"; ?>'">
																		<i class="ace-icon fa fa-check"></i>
																		Ya
																	</button>
																</div>
															</div>
														</div>
													</div>
												<?
												}
												?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <meta http-equiv="refresh" content="0; URL='<?php echo"$row_setting[domain_admin]"; ?>/masuk'" /> <? } ?>