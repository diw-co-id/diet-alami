<?php
include("connect_server.php");

if($_COOKIE['id_admin'] != 0)
{
	if(isset($_POST['button_submit']))
	{
		if (!empty($_FILES["gambar_foto_ramping_herbal"]["tmp_name"]))
		{
			$file_size = $_FILES['gambar_foto_ramping_herbal']['size'];
			$jenis_gambar = $_FILES['gambar_foto_ramping_herbal']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 10000000))
				{
					$message_tambah_foto_ramping_herbal = "Ukuran Gambar Maksimal 10mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['gambar_foto_ramping_herbal']['name']));
					if (move_uploaded_file($_FILES['gambar_foto_ramping_herbal']['tmp_name'], '/home/diec3486/public_html/images/foto-ramping-herbal/'.$gambar))
					{
						mysql_query("INSERT INTO foto_ramping_herbal (gambar_foto_ramping_herbal) VALUES ('$gambar')");
							
						$message_tambah_foto_ramping_herbal = "sukses";
						?>
						<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/foto_ramping_herbal"</script>
						<?
					}
					else
					{
						$message_tambah_foto_ramping_herbal = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_tambah_foto_ramping_herbal = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			$message_tambah_foto_ramping_herbal = "Gambar tidak boleh kosong.";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $judul = 'Tambah Foto Ramping Herbal (Admin Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li><i class="ace-icon fa fa-picture-o home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/foto_ramping_herbal">Foto Ramping Herbal</a></li>
							<li class="active"><i class="ace-icon fa fa-plus home-icon fa-fw"></i> Tambah Foto Ramping Herbal</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-edit home-icon fa-fw"></i> Tambah Foto Ramping Herbal</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							<?php
							if($message_tambah_foto_ramping_herbal != "" && $message_tambah_foto_ramping_herbal != "sukses")
							{
							?>
								<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-warning"></i> <?php echo"$message_tambah_foto_ramping_herbal"; ?>
								</div>
							<?
							}
							else if($message_tambah_foto_ramping_herbal == "sukses")
							{
							?>
								<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-check"></i> Berhasil, Foto Ramping Herbal telah ditambah.
								</div>
							<?
							}
							?>
								<form class="form-horizontal" role="form" name="tambah_foto_ramping_herbal" action="<?php echo"$row_setting[domain_admin]"; ?>/tambah_foto_ramping_herbal/<?php echo"$row_foto_ramping_herbal[id_foto_ramping_herbal]"; ?>" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Gambar Foto Ramping Herbal</label>

										<div class="col-sm-6">
											<input type="file" class="form-control" id="id-input-file-2" name="gambar_foto_ramping_herbal" maxlength="100" data-rel="tooltip" data-placement="top" title="Wajib Gambar, Format .PNG .JPG .JPEG, Ukuran Max 1mb" placeholder="Masukan Gambar Foto Ramping Herbal..." required />
										</div>
									</div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="button_submit" type="submit">
												<i class="ace-icon fa fa-plus bigger-110 fa-fw"></i>
												Tambah Foto Ramping Herbal
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110 fa-fw"></i>
												Reset
											</button>
										</div>
									</div>
								</form><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>