			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							&copy; Hak Cipta <?php echo date("Y"); ?> &reg; <a draggable="false" style="text-decoration: none;" href="<?php echo"$row_setting[domain]"; ?>/" target="_blank"><strong><?php echo"$row_setting[name_website]"; ?></strong></a> &trade;. Hak Cipta Dilindungi.
						</span>
					</div>
				</div>
			</div>

			<a draggable="false" href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-150"></i>
			</a>