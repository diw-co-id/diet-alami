<?php
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Testimoni | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body class="size-1140">
  
		<?php include("header.php"); ?>
	
		<main role="main">
			<article>
				<header class="section background-primary text-center">
					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Testimoni</h1>
				</header>
				<div class="section background-white"> 
					<div class="line">
						<div class="s-12 m-12 l-12">
    						<p class="text-size-20 text-center text-s-size-16 text-thin" style="color:#000000;">
    						    *Disclaimer : hasil dapat berbeda untuk tiap konsumen
    						</p>
    						<hr>
					        <div class="margin">
    						<?
    						$result_testimoni = mysql_query("SELECT * FROM testimoni ORDER BY id_testimoni DESC");
        					while($row_testimoni = mysql_fetch_array($result_testimoni))
        					{
    						?>
    							<div class="s-12 m-12 l-4">
    								<div class="image-with-hover-overlay image-hover-zoom margin-bottom">
    									<div class="image-hover-overlay background-primary"> 
    										<div class="image-hover-overlay-content text-center padding-2x">
    											<p>Testimoni <?php echo"$row_testimoni[id_testimoni]"; ?></p>  
    										</div> 
    									</div> 
    									<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/testimoni/<?php echo"$row_testimoni[gambar_testimoni]"; ?>" alt="Testimoni <?php echo"$row_testimoni[id_testimoni]"; ?> - <?php echo"$row_setting[Title]"; ?>" />
    								</div>
    							</div>
    						<?
    						}
    						?>
    						</div>
    						<br>
    						<p class="text-size-20 text-center text-s-size-16 text-thin" style="color:#000000;">
    						    *Disclaimer : hasil dapat berbeda untuk tiap konsumen
    						</p>
						</div>
					</div>
				</div>
			</article>
		</main>
		
		<?php include("footer.php"); ?>
		
	</body>
</html>