<?php
include("connect_server.php");

$result_slider = mysql_query("SELECT * FROM slider WHERE id_slider = '$_GET[id_slider]'");
$row_slider = mysql_fetch_array($result_slider);

if($_COOKIE['id_admin'] != 0)
{
	if(isset($_POST['button_submit']))
	{
		if (!empty($_FILES["gambar_slider"]["tmp_name"]))
		{
			$file_size = $_FILES['gambar_slider']['size'];
			$jenis_gambar = $_FILES['gambar_slider']['type'];
			if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
			{
				if (($file_size > 10000000))
				{
					$message_ubah_slider = "Ukuran Gambar Maksimal 10mb.";
				}
				else
				{
					$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['gambar_slider']['name']));
					if (move_uploaded_file($_FILES['gambar_slider']['tmp_name'], '/home/diec3486/public_html/images/slider/'.$gambar))
					{
						mysql_query("UPDATE slider SET gambar_slider = '$gambar' WHERE id_slider = '$row_slider[id_slider]'");
							
						$message_ubah_slider = "sukses";
						?>
						<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/slider"</script>
						<?
					}
					else
					{
						$message_ubah_slider = "Gambar Gagal Dikirim. Coba Lagi.";
					}
				}
			}
			else
			{
				$message_ubah_slider = "Format Gambar Salah, Wajib .PNG .JPG";
			}
		}
		else
		{
			$message_ubah_slider = "Mohon isi data yang kosong.";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $judul = 'Ubah Slider (Admin Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li><i class="ace-icon fa fa-sliders home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/slider">Slider</a></li>
							<li class="active"><i class="ace-icon fa fa-edit home-icon fa-fw"></i> Ubah Slider</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-edit home-icon fa-fw"></i> Ubah Slider</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							<?php
							if($message_ubah_slider != "" && $message_ubah_slider != "sukses")
							{
							?>
								<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-warning"></i> <?php echo"$message_ubah_slider"; ?>
								</div>
							<?
							}
							else if($message_ubah_slider == "sukses")
							{
							?>
								<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-check"></i> Berhasil, Slider telah diubah.
								</div>
							<?
							}
							?>
								<form class="form-horizontal" role="form" name="ubah_slider" action="<?php echo"$row_setting[domain_admin]"; ?>/ubah_slider/<?php echo"$row_slider[id_slider]"; ?>/" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Gambar Slider</label>

										<div class="col-sm-6">
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/slider/<?php echo"$row_slider[gambar_slider]"; ?>" width="100%"><hr>
											<input type="file" class="form-control" id="id-input-file-2" name="gambar_slider" maxlength="100" data-rel="tooltip" data-placement="top" title="KOSONGKAN JIKA TIDAK INGIN DIUBAH! Wajib Gambar, Format .PNG .JPG .JPEG, Ukuran Max 1mb" placeholder="Masukan Gambar Slider..." />
										</div>
									</div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="button_submit" type="submit">
												<i class="ace-icon fa fa-check bigger-110 fa-fw"></i>
												Simpan Perubahan
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110 fa-fw"></i>
												Reset
											</button>
										</div>
									</div>
								</form><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>