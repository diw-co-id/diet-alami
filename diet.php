<?php
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title>Artikel : Diet | <?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body class="size-1140">
  
		<?php include("header.php"); ?>
	
		<main role="main">
			<article>
				<header class="section background-primary text-center">
					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Artikel : Diet</h1>
				</header>
				<div class="section background-white"> 
					<div class="line">
						<div class="s-12 m-12 l-12">
					        <div class="margin">
    						<?
    						$result_artikel = mysql_query("SELECT * FROM artikel WHERE kategori_artikel = 'Diet' ORDER BY id_artikel DESC");
        					while($row_artikel = mysql_fetch_array($result_artikel))
        					{
    						?>
    							<div class="s-6 m-6 l-6">
    								<div class="image-with-hover-overlay image-hover-zoom margin-bottom">
    									<a draggable="false" href="<?php echo"$row_artikel[domain]"; ?>/artikel/<?php echo"$row_artikel[id_artikel]"; ?>"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/artikel/<?php echo"$row_artikel[gambar_artikel]"; ?>" alt="Artikel <?php echo"$row_artikel[judul_artikel]"; ?> - <?php echo"$row_setting[Title]"; ?>" /></a>
    								</div>
    								<h6 class="headline text-thin text-s-size-30">
    									<a draggable="false" href="<?php echo"$row_artikel[domain]"; ?>/artikel/<?php echo"$row_artikel[id_artikel]"; ?>"><b><?php echo"$row_artikel[judul_artikel]"; ?></b></a>
    								</h6>
    								<p style="white-space:pre-wrap; text-align:justify;"><?php echo substr("$row_artikel[deskripsi_artikel]",0,300); ?>&hellip;</p><br>
    								<a draggable="false" class="button border-radius background-primary text-size-12 text-white text-strong" href="<?php echo"$row_artikel[domain]"; ?>/artikel/<?php echo"$row_artikel[id_artikel]"; ?>">Baca Selengkapnya</a><hr>
    							</div>
    						<?
    						}
    						?>
    						</div>
						</div>
					</div>
				</div> 
			</article>
		</main>
		
		<?php include("footer.php"); ?>
		
	</body>
</html>