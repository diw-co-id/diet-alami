<?php
include("connect_server.php");

if($_COOKIE['id_admin'] != 0)
{
	if(isset($_POST['button_submit']))
	{
		$username_admin = $_POST['username_admin'];
		$nama_admin = $_POST['nama_admin'];

		$password_admin_md5 = $_POST['password_admin'];
		$password_admin = md5($password_admin_md5);
	
		if($username_admin != '' || $password_admin != '' || $nama_admin != '')
		{
			if (!empty($_FILES["foto_admin"]["tmp_name"]))
			{
				$file_size = $_FILES['foto_admin']['size'];
				$jenis_gambar = $_FILES['foto_admin']['type'];
				if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
				{
					if (($file_size > 10000000))
					{
						$message_tambah_admin = "Ukuran Gambar Maksimal 10mb.";
					}
					else
					{
						$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['foto_admin']['name']));
						if (move_uploaded_file($_FILES['foto_admin']['tmp_name'], '/home/diec3486/public_html/admin/images/admin/'.$gambar))
						{
							mysql_query("INSERT INTO admin (username_admin, password_admin, nama_admin, foto_admin) VALUES ('$username_admin', '$password_admin', '$nama_admin', '$gambar')");
								
							$message_tambah_admin = "sukses";
							?>
							<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/admin"</script>
							<?
						}
						else
						{
							$message_tambah_admin = "Gambar Gagal Dikirim. Coba Lagi.";
						}
					}
				}
				else
				{
					$message_tambah_admin = "Format Gambar Salah, Wajib .PNG .JPG";
				}
			}
			else
			{
				mysql_query("INSERT INTO admin (username_admin, password_admin, nama_admin, foto_admin) VALUES ('$username_admin', '$password_admin', '$nama_admin', 'user.png')");
								
				$message_tambah_admin = "sukses";
				?>
				<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/admin"</script>
				<?
			}
		}
		else
		{
			$message_tambah_admin = "Mohon isi data yang kosong.";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $judul = 'Tambah Admin (Admin Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li><i class="ace-icon fa fa-user home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/admin">Admin</a></li>
							<li class="active"><i class="ace-icon fa fa-plus home-icon fa-fw"></i> Tambah Admin</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-plus home-icon fa-fw"></i> Tambah Admin</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							<?php
							if($message_tambah_admin != "" && $message_tambah_admin != "sukses")
							{
							?>
								<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-warning"></i> <?php echo"$message_tambah_admin"; ?>
								</div>
							<?
							}
							else if($message_tambah_admin == "sukses")
							{
							?>
								<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-check"></i> Berhasil, admin telah ditambah.
								</div>
							<?
							}
							?>
								<form class="form-horizontal" role="form" name="tambah_admin" action="<?php echo"$row_setting[domain_admin]"; ?>/tambah_admin" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="username_admin">Username Admin</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="username_admin" name="username_admin" maxlength="50" data-rel="tooltip" data-placement="top" title="Max Char 50" placeholder="Masukan Username Admin..." required />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="password_admin">Password Admin</label>

										<div class="col-sm-6">
											<input type="password" class="form-control" id="password_admin" name="password_admin" maxlength="50" data-rel="tooltip" data-placement="top" title="Max Char 50" placeholder="Masukan Password Admin..." required />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="nama_admin">Nama Admin</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="nama_admin" name="nama_admin" maxlength="50" data-rel="tooltip" data-placement="top" title="Max Char 50" placeholder="Masukan Nama Admin..." required />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Foto Admin</label>

										<div class="col-sm-3">
											<input type="file" class="form-control" id="id-input-file-2" name="foto_admin" maxlength="100" data-rel="tooltip" data-placement="top" title="Wajib Gambar, Format .PNG .JPG .JPEG, Ukuran Max 1mb" placeholder="Masukan Foto Admin..." />
										</div>
									</div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="button_submit" type="submit">
												<i class="ace-icon fa fa-check bigger-110 fa-fw"></i>
												Tambah Admin
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110 fa-fw"></i>
												Reset
											</button>
										</div>
									</div>
								</form><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>