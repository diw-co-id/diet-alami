<?php
include("connect_server.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<title><?php echo"$row_setting[title]"; ?></title>
		
		<?php include("meta.php"); ?>
	</head>
	<body class="size-1140">
  
		<?php include("header.php"); ?>
	
		<main role="main">
			
			<?php include("slider.php"); ?>
			
			<!--<section class="section background-primary text-center">
				<div class="line">
					<div class="s-12 m-10 l-8 center">
						<h2 class="headline text-thin text-s-size-30">We are Web Design Heroes</h2>
						<p class="text-size-20">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis</p>
					</div>
				</div>
			</section>-->
			
			<article>
				<header class="section background-primary text-center">
					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Diet Alami - Cara Menurunkan Berat Badan Aman dan Cepat</h1>
				</header>
			</article>
			
			<section class="section background-white">
				<div class="margin text-center">
					<div class="line">
						<div class="s-12 m-12 l-5">
							<center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/susah-diet.jpg" alt="Susah Diet - <?php echo"$row_setting[title]"; ?>"></center>
						</div>
						<div class="s-12 m-12 l-7">
							<p class="text-size-20 text-s-size-16 text-thin" style="color:#FF0000;">
							    <br><br>
								<b>Apa ini yang sedang anda alami ?</b><br>
								<br>
								~ MERASA TIDAK NYAMAN DENGAN BERAT BADAN BERLEBIHAN ?<br>
								<br>
								~ MENCARI CARA MENURUNKAN BERAT BADAN DAN MEMILIKI TUBUH IDEAL DAN SEHAT ?<br>
								<br>
								~ BERAT BADAN NAIK SETELAH MELAHIRKAN ?<br>
								<br>
								~ SUDAH DIET DAN OLAHRAGA TETAP GAGAL ?<br>
								<br>
								~ SUDAH MENCOBA PRODUK PELANGSING LAIN SAMPAI JUTAAN RUPIAH TETAP GAGAL ?
							</p>
						</div>
					</div>
				</div>
			</section>
			
			<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-8">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Diet Alami Solusinya!</h1>
            					<br>
            					<center><img draggable="false" style="width:200px;" src="<?php echo"$row_setting[domain]"; ?>/images/logo-best-seller.png" alt="Logo Best Seller - <?php echo"$row_setting[title]"; ?>"></center>
            					<br>
            					<h4 class="headline text-thin text-s-size-30">
            						<b>100% HERBAL<br>
            						100% ALAMI<br>
            						100% AMAN<br>
            						SUDAH BANYAK YANG BERHASIL !!</b>
            					</h4>
						    </div>
    						<div class="s-12 m-12 l-4">
						        <center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/beranda-1.jpg" alt="Logo Best Seller - <?php echo"$row_setting[title]"; ?>"></center>
							    <center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/beranda-2.jpg" alt="Logo Best Seller - <?php echo"$row_setting[title]"; ?>"></center>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<section class="section background-white">
				<div class="margin">
					<div class="line">
						<div class="s-12 m-12 l-4">
							<p class="text-size-20 text-s-size-16 text-thin" style="color:#000000;">
								<b style="color:#FF0000;">Jamu Ramping Herbal</b><br>
								<br>
								&bull; Terbuat Dari Bahan Herbal 100% Aman<br>
								&bull; Terdaftar Depkes RI ( BPOM )<br>
								&bull; Tanpa Efek Samping<br>
								&bull; Tersedia Bentuk KAPSUL<br>
								&bull; Saat Nya Diet Sehat<br>
								<br><br>
							</p>
						</div>
						<div class="s-12 m-12 l-8">
							<p class="text-size-20 text-s-size-16 text-thin" style="color:#FF0000;">
								<b style="color:#FF0000;">Manfaat Ramping Herbal :</b><br>
								<br>
								&bull; Membantu Menurunkan Berat Badan dan Membantu Mengencangkan Tubuh<br>
								&bull; Membantu Mengecilkan Lengan , Paha , Betis , Perut , Pantat dan Bagian Tubuh Lain Yang Terdapat Lemak Berlebih<br>
								&bull; Mebantu Melancarkan Pencernaan dan BAB Untuk Yang Susah BAB<br>
								&bull; Dapat di Stop Kapan Saja dan Tidak Membuat Ketergantungan Bagi Tubuh<br>
								&bull; Membantu Tubuh Menjadi Sehat , Ideal dan Lincah<br>
								*Disclaimer : hasil dapat berbeda untuk tiap konsumen
							</p>
						</div>
					</div>
				</div>
			</section>
			
			<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-12">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Testimoni Yang Sudah Berhasil!</h1>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<section class="section background-white">
				<div class="margin text-center">
					<div class="line">
						<div class="s-12 m-12 l-12">
					        <div class="margin">
    						<?
    						$result_testimoni = mysql_query("SELECT * FROM testimoni WHERE id_testimoni IN ('1','4','5') ORDER BY id_testimoni ASC");
        					while($row_testimoni = mysql_fetch_array($result_testimoni))
        					{
    						?>
    							<div class="s-12 m-12 l-4">
    								<div class="image-with-hover-overlay image-hover-zoom margin-bottom">
    									<div class="image-hover-overlay background-primary"> 
    										<div class="image-hover-overlay-content text-center padding-2x">
    											<p>Testimoni <?php echo"$row_testimoni[id_testimoni]"; ?></p>  
    										</div> 
    									</div> 
    									<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/testimoni/<?php echo"$row_testimoni[gambar_testimoni]"; ?>" alt="Testimoni <?php echo"$row_testimoni[id_testimoni]"; ?> - <?php echo"$row_setting[Title]"; ?>" />
    								</div>	
    							</div>
    						<?
    						}
    						?>
    						</div>
						</div>
						<p class="text-size-20 text-s-size-16 text-thin" style="color:#000000;">
						    *Disclaimer : hasil dapat berbeda untuk tiap konsumen
						</p>
					</div>
				</div>
			</section>
			
			<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-12">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Terdaftar BPOM RI</h1><br>
            					<br>
            					<h5 class="headline text-thin text-s-size-30">
									<b>TIDAK PERLU OLAHRAGA !!<br>
									TIDAK PERLU DIET !!<br>
									TIDAK ADA PANTANGAN !!<br>
									<br>
									<i>Pesan Sekarang Sebelum Promo Habis.</i></b>
								</h5>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<?php include("order-form.php"); ?>
			
			<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-12">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Tersedia juga di marketplace</h1><br>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<section class="section background-white">
				<div class="margin text-center">
					<div class="line">
						<div class="s-12 m-12 l-12">
					        <div class="margin">
    							<div class="s-12 m-12 l-4">
    								<a draggable="false" href="https://www.tokopedia.com/rampingherbal/ramping-herbal-1?trkid=f=Ca0000L000P0W0S0Sh00Co0Po0Fr0Cb0_src=shop-product_page=1_ob=11_q=_catid=598_po=1" target="_blank"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-tokopedia.png" alt="Logo Tokopedia - <?php echo"$row_setting[Title]"; ?>" /></a>
    							</div>
    							<div class="s-12 m-12 l-4">
    								<a draggable="false" href="https://www.bukalapak.com/p/kesehatan-2359/obat-suplemen/herbal/1becc6-jual-rampingherbal" target="_blank"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-bukalapak.png" alt="Logo Bukalapak - <?php echo"$row_setting[Title]"; ?>" /></a>
    							</div>
    							<div class="s-12 m-12 l-4">
    								<a draggable="false" href="https://shopee.co.id/Jamu-Ramping-Herbal-i.10834077.74016441" target="_blank"><img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/logo-shopee.png" alt="Logo Shopee - <?php echo"$row_setting[Title]"; ?>" /></a>
    							</div>
    						</div>
						</div>
					</div>
				</div>
			</section>
			
			<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-12">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Bukti Pengiriman Yang Telah Kami Antar</h1><br>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<section class="section background-white">
				<div class="margin text-center">
					<div class="line">
						<div class="s-12 m-12 l-12">
					        <div class="margin">
    						<?
    						$result_bukti_pengiriman = mysql_query("SELECT * FROM bukti_pengiriman WHERE id_bukti_pengiriman IN ('1','2','3') ORDER BY id_bukti_pengiriman DESC");
        					while($row_bukti_pengiriman = mysql_fetch_array($result_bukti_pengiriman))
        					{
    						?>
    							<div class="s-12 m-12 l-4">
    								<div class="image-with-hover-overlay image-hover-zoom margin-bottom">
    									<div class="image-hover-overlay background-primary"> 
    										<div class="image-hover-overlay-content text-center padding-2x">
    											<p>Bukti Pengiriman <?php echo"$row_bukti_pengiriman[id_bukti_pengiriman]"; ?></p>  
    										</div> 
    									</div> 
    									<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/bukti-pengiriman/<?php echo"$row_bukti_pengiriman[gambar_bukti_pengiriman]"; ?>" alt="Bukti Pengiriman <?php echo"$row_bukti_pengiriman[id_bukti_pengiriman]"; ?> - <?php echo"$row_setting[Title]"; ?>" />
    								</div>	
    							</div>
    						<?
    						}
    						?>
    						</div>
						</div>
					</div>
				</div>
			</section>
			
			<!--<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-12">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Foto Ramping Herbal - Terdaftar BPOM RI</h1><br>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<section class="section background-white">
				<div class="margin text-center">
					<div class="line">
						<div class="s-12 m-12 l-12">
					        <div class="margin">
    						<?
    						$result_foto_ramping_herbal = mysql_query("SELECT * FROM foto_ramping_herbal WHERE id_foto_ramping_herbal IN ('3','4','5') ORDER BY id_foto_ramping_herbal DESC");
        					while($row_foto_ramping_herbal = mysql_fetch_array($result_foto_ramping_herbal))
        					{
    						?>
    							<div class="s-12 m-12 l-4">
    								<div class="image-with-hover-overlay image-hover-zoom margin-bottom">
    									<div class="image-hover-overlay background-primary"> 
    										<div class="image-hover-overlay-content text-center padding-2x">
    											<p>Foto Ramping Herbal <?php echo"$row_foto_ramping_herbal[id_foto_ramping_herbal]"; ?></p>  
    										</div>
    									</div> 
    									<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/foto-ramping-herbal/<?php echo"$row_foto_ramping_herbal[gambar_foto_ramping_herbal]"; ?>" alt="Foto Ramping Herbal <?php echo"$row_foto_ramping_herbal[id_foto_ramping_herbal]"; ?> - <?php echo"$row_setting[Title]"; ?>" />
    								</div>
    							</div>
    						<?
    						}
    						?>
    						</div>
						</div>
						<br>
						<hr>
						<br>
						<div class="s-12 m-12 l-4">&nbsp;</div>
						<div class="s-12 m-12 l-4">
                	        <center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/24-jam.gif" alt="24 Jam - <?php echo"$row_setting[title]"; ?>"></center><br>
                			<center><img draggable="false" style="width:100%;" src="<?php echo"$row_setting[domain]"; ?>/images/pesan-sekarang.gif" alt="Pesan Sekarang - <?php echo"$row_setting[title]"; ?>"></center>
						</div>
						<div class="s-12 m-12 l-4">&nbsp;</div>
					</div>
				</div>
			</section>-->
			
			<!--<article>
				<header class="section background-primary text-center">
    				<div class="margin text-center">
    					<div class="line">
    						<div class="s-12 m-12 l-12">
            					<h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Pesan Sekarang Juga!</h1>
						    </div>
						</div>
					</div>
				</header>
			</article>
			
			<?php include("order-form.php"); ?>-->
			
			<hr class="break margin-top-bottom-0">
			
		</main>
		
		<?php include("footer.php"); ?>
		
	</body>
</html>