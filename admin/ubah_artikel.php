<?php
include("connect_server.php");

$result_artikel = mysql_query("SELECT * FROM artikel WHERE id_artikel = '$_GET[id_artikel]'");
$row_artikel = mysql_fetch_array($result_artikel);

if($_COOKIE['id_admin'] != 0)
{
	if(isset($_POST['button_submit']))
	{
		$kategori_artikel = $_POST['kategori_artikel'];
		$judul_artikel = $_POST['judul_artikel'];
		
		$deskripsi_artikel_escape_string = $_POST['deskripsi_artikel'];
		$deskripsi_artikel = mysql_escape_string($deskripsi_artikel_escape_string);
	
		if($kategori_artikel != '' || $judul_artikel != '' || $deskripsi_artikel_escape_string != '')
		{
			if (!empty($_FILES["gambar_artikel"]["tmp_name"]))
			{
				$file_size = $_FILES['gambar_artikel']['size'];
				$jenis_gambar = $_FILES['gambar_artikel']['type'];
				if($jenis_gambar=="image/jpeg" || $jenis_gambar=="image/jpg" || $jenis_gambar=="image/png")
				{
					if (($file_size > 5000000))
					{
						$message_ubah_artikel = "Ukuran Gambar Maksimal 5mb.";
					}
					else
					{
						$gambar = $namafolder . strtolower(str_replace(" ","_", $_FILES['gambar_artikel']['name']));
						if (move_uploaded_file($_FILES['gambar_artikel']['tmp_name'], '/home/diec3486/public_html/images/artikel/'.$gambar))
						{
							mysql_query("UPDATE artikel SET kategori_artikel = '$kategori_artikel', judul_artikel = '$judul_artikel', deskripsi_artikel = '$deskripsi_artikel', gambar_artikel = '$gambar' WHERE id_artikel = '$_GET[id_artikel]'");
								
							$message_ubah_artikel = "sukses";
							?>
							<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/artikel"</script>
							<?
						}
						else
						{
							$message_ubah_artikel = "Gambar Gagal Dikirim. Coba Lagi.";
						}
					}
				}
				else
				{
					$message_ubah_artikel = "Format Gambar Salah, Wajib .PNG .JPG";
				}
			}
			else
			{
				mysql_query("UPDATE artikel SET kategori_artikel = '$kategori_artikel', judul_artikel = '$judul_artikel', deskripsi_artikel = '$deskripsi_artikel' WHERE id_artikel = '$_GET[id_artikel]'");
					
				$message_ubah_artikel = "sukses";
				?>
				<script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/artikel"</script>
				<?
			}
		}
		else
		{
			$message_ubah_artikel = "Mohon isi data yang kosong.";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php include("copyright.php"); ?>
<html lang="id" itemscope itemtype="http://schema.org/WebPage" xmlns="http://www.w3.org/1999/xhtml" xml:lang="id">
	<head>
		<?php $judul = 'Ubah Artikel (Artikel Panel)'; ?>

		<title><?php echo"$judul"; ?> - <?php echo"$row_setting[name_website]"; ?></title>

		<?php include("meta.php"); ?>
	</head>

	<body class="no-skin">

		<?php include("header.php"); ?>

		<div class="main-container ace-save-state" id="main-container">

			<?php include("navigation.php"); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb" style="margin-top:10px;">
							<li><i class="ace-icon fa fa-dashboard home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/">Dashboard</a></li>
							<li><i class="ace-icon fa fa-newspaper-o home-icon fa-fw"></i> <a draggable="false" href="<?php echo"$row_setting[domain_admin]"; ?>/artikel">Artikel</a></li>
							<li class="active"><i class="ace-icon fa fa-edit home-icon fa-fw"></i> Ubah Artikel</li>
						</ul><!-- /.breadcrumb -->

						<?php include("header_search.php"); ?>
					</div>

					<div class="page-content">

						<?php include("menu_setting.php"); ?>

						<div class="page-header">
							<h1><i class="ace-icon fa fa-edit home-icon fa-fw"></i> Ubah Artikel</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							<?php
							if($message_ubah_artikel != "" && $message_ubah_artikel != "sukses")
							{
							?>
								<div class="alert alert-danger fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-warning"></i> <?php echo"$message_ubah_artikel"; ?>
								</div>
							<?
							}
							else if($message_ubah_artikel == "sukses")
							{
							?>
								<div class="alert alert-success fade in"> <a class="close" data-dismiss="alert" href="#">&times;</a>
									<i class="fa fa-fw fa-check"></i> Berhasil, Artikel telah diubah.
								</div>
							<?
							}
							?>
								<form class="form-horizontal" role="form" name="ubah_artikel" action="<?php echo"$row_setting[domain_admin]"; ?>/ubah_artikel/<?php echo"$row_artikel[id_artikel]"; ?>/" method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="kategori_artikel">Kategori Artikel</label>

										<div class="col-sm-6">
											<select class="chosen-select form-control" id="kategori_artikel" name="kategori_artikel" data-placeholder="Pilih Kategori Artikel..." required />
											<?
											if($row_artikel[kategori_artikel] == 'Pelangsing')
											{
											?>
												<option value="Pelangsing" selected>Pelangsing</option>
												<option value="Diet">Diet</option>
											<?
											}
											else
											{
											?>
												<option value="Pelangsing">Pelangsing</option>
												<option value="Diet" selected>Diet</option>
											<?
											}
											?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="judul_artikel">Judul Artikel</label>

										<div class="col-sm-6">
											<input type="text" class="form-control" id="judul_artikel" name="judul_artikel" maxlength="200" data-rel="tooltip" data-placement="top" title="Max Char 200" placeholder="Masukan Judul Artikel..." value="<?php echo"$row_artikel[judul_artikel]"; ?>" required />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="deskripsi_artikel">Deskripsi Artikel</label>

										<div class="col-sm-6">
											<textarea style="resize: none;" id="deskripsi_artikel" name="deskripsi_artikel" class="autosize-transition form-control" maxlength="20000" data-rel="tooltip" data-placement="top" title="Max Char 20000" placeholder="Masukan Deskripsi Artikel..." required /><?php echo"$row_artikel[deskripsi_artikel]"; ?></textarea>

										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2">Gambar Artikel</label>

										<div class="col-sm-3">
											<img draggable="false" src="<?php echo"$row_setting[domain]"; ?>/images/artikel/<?php echo"$row_artikel[gambar_artikel]"; ?>" width="100%"><hr>
											<input type="file" class="form-control" id="id-input-file-2" name="gambar_artikel" maxlength="100" data-rel="tooltip" data-placement="top" title="Wajib Gambar, Format .PNG .JPG .JPEG, Ukuran Max 1mb" placeholder="Masukan Gambar Artikel..." />
										</div>
									</div>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="button_submit" type="submit">
												<i class="ace-icon fa fa-check bigger-110 fa-fw"></i>
												Simpan Perubahan
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110 fa-fw"></i>
												Reset
											</button>
										</div>
									</div>
								</form><!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php include("footer.php"); ?>

		</div><!-- /.main-container -->

		<?php include("script.php"); ?>
	</body>
</html>
<? } else { ?> <script type="text/javascript">window.location = "<?php echo"$row_setting[domain_admin]"; ?>/masuk"</script> <? } ?>